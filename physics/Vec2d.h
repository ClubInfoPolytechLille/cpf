#pragma once

# define M_PI           3.14159265358979323846

class Vec2d {
private:
	double m_x, m_y;
public:
	Vec2d();
	Vec2d(const Vec2d& v);
	Vec2d(double x, double y);

	Vec2d& operator= (const Vec2d& v);
	Vec2d  operator+ (const Vec2d& v);
	Vec2d  operator- (const Vec2d& v);
	Vec2d& operator+=(const Vec2d& v);
	Vec2d& operator-=(const Vec2d& v);
	Vec2d& operator/=(const double s);
	Vec2d& operator*=(const double s);
	Vec2d  operator* (const double& s);
	Vec2d  operator/ (const double& s);
	Vec2d& normalize();
	Vec2d& rotate(double deg);
	bool   operator==(const Vec2d& v);
	void   set(double x, double y);
	void   setX(double x);
	void   setY(double x);
	double x();
	double y();
	double angle();
	double length();
	void truncate(double length);

	static double dot(Vec2d v1, Vec2d v2);
	static double cross(Vec2d v1, Vec2d v2);

};

