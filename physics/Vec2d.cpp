#include "Vec2d.h"
#include <cmath>

Vec2d::Vec2d() : m_x(0), m_y(0) {}

Vec2d::Vec2d(double x, double y) { 
	m_x = x;
	m_y = y;
}

Vec2d::Vec2d(const Vec2d& v) {
	m_x = v.m_x;
	m_y = v.m_y;
}

Vec2d& Vec2d::operator=(const Vec2d& v) {
	m_x = v.m_x;
	m_y = v.m_y;
	return *this;
}

Vec2d Vec2d::operator+(const Vec2d& v) {
	return Vec2d(m_x + v.m_x, m_y + v.m_y);
}

Vec2d Vec2d::operator-(const Vec2d& v) {
	return Vec2d(m_x - v.m_x, m_y - v.m_y);
}

Vec2d Vec2d::operator/(const double& s) {
	return Vec2d(m_x / s, m_y / s);
}

Vec2d Vec2d::operator*(const double& s) {
	return Vec2d(m_x * s, m_y * s);
}

Vec2d& Vec2d::operator+=(const Vec2d& v) {
	m_x += v.m_x;
	m_y += v.m_y;
	return *this;
}
Vec2d& Vec2d::operator-=(const Vec2d& v) {
	m_x -= v.m_x;
	m_y -= v.m_y;
	return *this;
}

Vec2d& Vec2d::operator/=(const double s) {
	m_x /= s;
	m_y /= s;
	return *this;
}

Vec2d& Vec2d::operator*=(const double s) {
	m_x *= s;
	m_y *= s;
	return *this;
}

bool Vec2d::operator==(const Vec2d& v) {
	if (v.m_x == this->m_x && v.m_y == this->m_y) {
		return true;
	}

	else {
		return false;
	}
}

void Vec2d::set(double x, double y) {
	this->m_x = x;
	this->m_y = y;
}

void Vec2d::setX(double x) {
	this->m_x = x;
}

void Vec2d::setY(double y) {
	this->m_y = y;
}

double Vec2d::dot(Vec2d v1, Vec2d v2) {
	return v1.x() * v2.x() + v1.y() * v2.y();
}

double Vec2d::cross(Vec2d v1, Vec2d v2) {
	return (v1.x() * v2.y()) - (v1.y() * v2.x());
}

double Vec2d::x() {
	return m_x;
}

double Vec2d::y() {
	return m_y;
}

double Vec2d::angle() {
	return atan2(m_y, m_x);
}

double Vec2d::length() {
	return sqrt(m_x * m_x + m_y * m_y);
}

Vec2d& Vec2d::rotate(double deg) {
	double rad = deg / 180.0 * M_PI;
	double c = cos(rad);
	double s = sin(rad);
	double tx = m_x * c - m_y * s;
	double ty = m_x * s + m_y * c;
	m_x = tx;
	m_y = ty;
	return *this;
}

Vec2d& Vec2d::normalize() {
	if (length() == 0) return *this;
	*this *= (1.0 / length());
	return *this;
}

void Vec2d::truncate(double length) {
	m_x = length * cos(angle());
	m_y = length * sin(angle());
}